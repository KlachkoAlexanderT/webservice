@RestResource(UrlMapping='/api/v1/*')
global with sharing class RestApi {
	@HttpPost
	global static void create() {
		RestContext.response.addHeader('Content-Type', 'application/json');
		if (RestContext.request.requestURI.containsIgnoreCase('/api/v1/upsertAccounts')) {
			String requestBody = RestContext.request.requestBody.toString();
			Map<String, Object> result = RestService.upsertAccounts(requestBody);
			RestContext.response.responseBody = Blob.valueOf(JSON.serialize(result));
		}
	}
}
