public with sharing class RestService {
	public static final String BONUS = 'BONUS_';

	public static Map<String, Object> upsertAccounts(String requestBody) {
		Map<String, String> keyToField = new Map<String, String> {
			'clientID' => 'PersonContactId',
			'transactionID' => 'TransactionID__c',
			'lastname' => 'LastName',
			'firstname' => 'FirstName',
			'email' => 'PersonEmail',
			'mobilePhone' => 'PersonMobilePhone',
			'birthdate' => 'PersonBirthdate',
			'postalcode' => 'PersonMailingPostalCode',
			'country' => 'PersonMailingCountry',
			'city' => 'PersonMailingCity',
			'street' => 'PersonMailingStreet'
		};
		List<Map<String, Object>> clientsResponse = new List<Map<String, Object>>();
		String resultMessage;
		Map<String, Object> params = (Map<String, Object>)JSON.deserializeUntyped(requestBody);
		String source = (String)params.get('source');
		List<Object> clients = (List<Object>)params.get('clients');
		Map<String, Object> response = new Map<String, Object>();
		Savepoint sp;
		try {
			List<Map<String, Object>> fieldToValues = new List<Map<String, Object>>();
			Map<String, String> transactionIdToBonusKey = new Map<String, String>();
			List<Account> accounts = new List<Account>();
			for (Integer i = 0; i < clients.size(); i++) {
				Map<String, Object> fieldToValue = (Map<String, Object>)clients[i];
				transactionIdToBonusKey.put((String)fieldToValue.get('transactionID'), (String)fieldToValue.get('bonusKey'));
				fieldToValue.remove('bonusKey');
				String str = JSON.serialize(fieldToValue);
				for (String key: keyToField.keySet()){
					String fieldName = '"' + keyToField.get(key) + '"';
					String regExp = '"' + key + '"';
					str = str.replaceAll(regExp, fieldName);
				}
				Account accountRecord = (Account)JSON.deserialize(str, Account.class);
				accountRecord.Source__c = source;
				accounts.add(accountRecord);
			}
			sp = Database.setSavepoint();
			Database.UpsertResult[] upsertResults = Database.upsert(accounts, Account.Fields.TransactionID__c, false);
			Map<Id, Account> idToAccount = new Map<Id, Account> (
				[
					SELECT Id, PersonContactId, TransactionID__c, (SELECT Id, Bonus__c FROM PersonalAccountsToBonuses__r)
					FROM Account 
					WHERE Id IN :accounts
				]
			);
			Set<String> apiNames = Utility.getAPINamesPicklist(Bonus__c.Region__c);
			Set<String> additionalBonusKeys = new Set<String>();
			for (String apiName :apiNames) {
				additionalBonusKeys.add(RestService.BONUS + apiName);
			}
			List<Bonus__c> bonuses = [
				SELECT Id, Region__c, Ext_Id__c
				FROM Bonus__c WHERE Ext_Id__c IN :transactionIdToBonusKey.values() OR Ext_Id__c IN :additionalBonusKeys
			];
			Map<String, Bonus__c> extIdToBonus = new Map<String, Bonus__c>();
			for (Bonus__c bonus :bonuses) {
				extIdToBonus.put(bonus.Ext_Id__c, bonus);
			}
			Map<Id, Set<Id>> accountIdToBonuses = new Map<Id, Set<Id>>();
			List<PersonalAccountToBonus__c> personalAccountToBonuses = new List<PersonalAccountToBonus__c>();
			for (Account accountItem : idToAccount.values()) {
				Set<Id> bonusesOfPerson = new Set<Id>();
				for (PersonalAccountToBonus__c personalAccountsToBonus :accountItem.PersonalAccountsToBonuses__r) {
					bonusesOfPerson.add(personalAccountsToBonus.Bonus__c);
				}
				accountIdToBonuses.put(accountItem.Id, bonusesOfPerson);
				Bonus__c bonus;
				if (transactionIdToBonusKey.containsKey(accountItem.TransactionID__c)) {
					bonus = extIdToBonus.get(transactionIdToBonusKey.get(accountItem.TransactionID__c));
				}
				if (bonus != null && !bonusesOfPerson.contains(bonus.Id)) {
					personalAccountToBonuses.add(
						new PersonalAccountToBonus__c(
							Account__c = accountItem.Id,
							Bonus__c = bonus.Id
						)
					);
					String nameAdditionalBonus = RestService.BONUS + bonus.Region__c;
					Bonus__c additionalBonus = extIdToBonus.get(nameAdditionalBonus);
					if (additionalBonus != null && !bonusesOfPerson.contains(additionalBonus.Id)) {
						personalAccountToBonuses.add(
							new PersonalAccountToBonus__c(
								Account__c = accountItem.Id,
								Bonus__c = additionalBonus.Id
							)
						);
					}
				}
			}
			insert personalAccountToBonuses;
			Integer countError = 0;
			for (Integer i = 0; i < upsertResults.size(); i++) {
				Map<String, Object> clientResponse = new Map<String, Object>();
				if (!upsertResults[i].isSuccess()) {
					countError++;
					String errorMsg = '';
					for (Database.Error err : upsertResults[i].getErrors()) {
						errorMsg += err.getStatusCode() + ': ' + err.getMessage() + ' ';
					}
					clientResponse.put('clientID', accounts[i].PersonContactId);
					clientResponse.put('transactionID', accounts[i].TransactionID__c);
					clientResponse.put('resultMessage', errorMsg);
					clientResponse.put('resultCode', 302);
				} else {
					clientResponse.put('clientID', idToAccount.get(accounts[i].Id).PersonContactId);
					clientResponse.put('transactionID', idToAccount.get(accounts[i].Id).TransactionID__c);
					clientResponse.put('resultMessage', 'OK');
					clientResponse.put('resultCode', 200);
				}
				clientsResponse.add(clientResponse);
			}
			resultMessage = (accounts.size() + ' clients were tried to be inserted/updated. ' + countError + ' with error');
		} catch (Exception ex) {
			Database.rollback(sp);
			Integer clientSize = clients.size();
			for (Integer i = 0; i < clientSize; i++) {
				Map<String, Object> clientResponse = new Map<String, Object>();
				Map<String, Object> fieldToValue = (Map<String, Object>)clients[i];
				clientResponse.put('clientID', fieldToValue.get('clientID'));
				clientResponse.put('transactionID', fieldToValue.get('transactionID'));
				clientResponse.put('resultMessage', ex.getMessage());
				clientResponse.put('resultCode', 302);
				clientsResponse.add(clientResponse);
			}
			resultMessage = (clientSize + ' clients were tried to be inserted/updated. ' + clientSize + ' with error');
		}
		response = new Map<String, Object>{
			'resultMessage' => resultMessage,
			'clients' => clientsResponse
		};
		return response;
	}
}
