@IsTest
private class RestApiTest {
	static testmethod void upsertAccountsTest() {
		Bonus__c bonusRU = new Bonus__c();
		bonusRU.Ext_Id__c = 'BONUS_RU';
		bonusRU.Region__c = 'RU';
		Bonus__c bonusRU_111 = new Bonus__c();
		bonusRU_111.Ext_Id__c = 'exRU_111';
		bonusRU_111.Region__c = 'RU';
		Bonus__c bonusRU_222 = new Bonus__c();
		bonusRU_222.Ext_Id__c = 'exRU_222';
		bonusRU_222.Region__c = 'RU';
		Bonus__c bonusRU_333 = new Bonus__c();
		bonusRU_333.Ext_Id__c = 'exRU_333';
		bonusRU_333.Region__c = 'RU';
		insert new List<Bonus__c> {
			bonusRU, bonusRU_111, bonusRU_222, bonusRU_333
		};
		Account accountA = new Account();
		accountA.Source__c = 'C';
		accountA.FirstName = 'A-Test';
		accountA.TransactionID__c = 'A-TransactionID';
		accountA.PersonEmail = 'a@gmail.com';
		accountA.LastName = 'A-Test';
		insert new List<Account> {
			accountA
		};
		PersonalAccountToBonus__c personalAccountToBonusA = new PersonalAccountToBonus__c();
		personalAccountToBonusA.Account__c = accountA.Id;
		personalAccountToBonusA.Bonus__c = bonusRU_111.Id;
		PersonalAccountToBonus__c personalAccountToBonusA_RU = new PersonalAccountToBonus__c();
		personalAccountToBonusA_RU.Account__c = accountA.Id;
		personalAccountToBonusA_RU.Bonus__c = bonusRU.Id;
		insert new List<PersonalAccountToBonus__c> {
			personalAccountToBonusA, personalAccountToBonusA_RU
		};
		Map<Id, Account> idToAccount = new Map<Id, Account>([SELECT Id, PersonContactId FROM Account]);
		String requestBody = 
		'{' +
			'"source":"A",' +
			'"clients":[' +
				'{' +
					'"clientID":"' + idToAccount.get(accountA.Id).PersonContactId + '",' +
					'"transactionID":"' + accountA.TransactionID__c + '",' +
					'"bonusKey":"' + bonusRU_111.Ext_Id__c + '",' +
					'"lastname": "A-Test1",' + 
					'"firstname": "A-Test2",' + 
					'"email": "a@gmail.com",' + 
					'"mobilePhone": "+2365178951",' + 
					'"birthdate": "2020-10-02",' + 
					'"postalcode": "224000",' + 
					'"country": "Belarus",' + 
					'"city": "Brest",' + 
					'"street": "Lenina"' +
				'},' +
				'{' +
					'"clientID":"",' +
					'"transactionID":"C-TransactionID",' +
					'"bonusKey":"' + bonusRU_333.Ext_Id__c + '",' +
					'"lastname": "C-Test1",' + 
					'"firstname": "C-Test2",' + 
					'"email": "a@gmail.com",' + 
					'"mobilePhone": "+2365178951",' + 
					'"birthdate": "2020-10-02",' + 
					'"postalcode": "224000",' + 
					'"country": "Belarus",' + 
					'"city": "Gomel",' + 
					'"street": "Lenina"' +
				'}' +
			']' +
		'}';
		RestRequest request = new RestRequest();
		request.requestUri = Url.getOrgDomainUrl().toExternalForm() + '/services/apexrest/api/v1/upsertAccounts/';

		request.addHeader('Content-Type', 'application/json');
		request.requestBody = Blob.valueOf(requestBody);
		request.httpMethod = 'POST';
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;
		RestApi.create();
		String responseBody = RestContext.response.responseBody.toString();
		Map<String, Object> result = (Map<String, Object>)JSON.deserializeUntyped(responseBody);
		System.assertEquals('2 clients were tried to be inserted/updated. 0 with error', (String)result.get('resultMessage'));
		List<Account> accounts = [
			SELECT Id, PersonContactId, TransactionID__c, LastName, FirstName, PersonMailingCity, PersonEmail, PersonMobilePhone, 
				PersonMailingPostalCode, PersonMailingStreet, (SELECT Id, Bonus__c FROM PersonalAccountsToBonuses__r)
			FROM Account
		];
		Map<String, Account> transactionIDToAccount = new Map<String, Account>();
		for (Account account :accounts) {
			transactionIDToAccount.put(account.TransactionID__c, account);
		}
		List<Object> clients = (List<Object>)result.get('clients');
		Integer countError = 0;
		for (Object client :clients) {
			Map<String, Object> nameToValue = (Map<String, Object>)client;
			String transactionID = (String) nameToValue.get('transactionID');
			switch on transactionID {
				when 'A-TransactionID' {
					System.assertEquals('A-Test1', transactionIDToAccount.get(transactionID).LastName);
					System.assertEquals('A-Test2', transactionIDToAccount.get(transactionID).FirstName);
					System.assertEquals('Brest', transactionIDToAccount.get(transactionID).PersonMailingCity);
					System.assertEquals('a@gmail.com', transactionIDToAccount.get(transactionID).PersonEmail);
					System.assertEquals('+2365178951', transactionIDToAccount.get(transactionID).PersonMobilePhone);
					System.assertEquals('224000', transactionIDToAccount.get(transactionID).PersonMailingPostalCode);
					System.assertEquals('Lenina', transactionIDToAccount.get(transactionID).PersonMailingStreet);
					System.assertEquals(2, transactionIDToAccount.get(transactionID).PersonalAccountsToBonuses__r.size());
					Set<Id> bonuses = new Set<Id> {
						transactionIDToAccount.get(transactionID).PersonalAccountsToBonuses__r[0].Bonus__c,
						transactionIDToAccount.get(transactionID).PersonalAccountsToBonuses__r[1].Bonus__c
					};
					System.assert(bonuses.contains(bonusRU_111.Id));
					System.assert(bonuses.contains(bonusRU.Id));
				}
				when 'C-TransactionID' {
					System.assertEquals('C-Test1', transactionIDToAccount.get(transactionID).LastName);
					System.assertEquals('C-Test2', transactionIDToAccount.get(transactionID).FirstName);
					System.assertEquals('Gomel', transactionIDToAccount.get(transactionID).PersonMailingCity);
					System.assertEquals('a@gmail.com', transactionIDToAccount.get(transactionID).PersonEmail);
					System.assertEquals('+2365178951', transactionIDToAccount.get(transactionID).PersonMobilePhone);
					System.assertEquals('224000', transactionIDToAccount.get(transactionID).PersonMailingPostalCode);
					System.assertEquals('Lenina', transactionIDToAccount.get(transactionID).PersonMailingStreet);
					System.assertEquals(2, transactionIDToAccount.get(transactionID).PersonalAccountsToBonuses__r.size());
					Set<Id> bonuses = new Set<Id> {
						transactionIDToAccount.get(transactionID).PersonalAccountsToBonuses__r[0].Bonus__c,
						transactionIDToAccount.get(transactionID).PersonalAccountsToBonuses__r[1].Bonus__c
					};
					System.assert(bonuses.contains(bonusRU_333.Id));
					System.assert(bonuses.contains(bonusRU.Id));
				}
			}
		}
	}
}
