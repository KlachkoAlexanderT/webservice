public with sharing class TriggerHandler_Account {
	public static void setRecordType(List<Account> accounts, Map<Id, Account> idToAccountOld) {
		for (Account accountItem :accounts) {
			if (accountItem.Source__c != null &&
				(
					idToAccountOld == null || accountItem.Source__c != idToAccountOld.get(accountItem.Id).Source__c
				) && 
				Utility.getRecordTypeIdByDeveloperName(Account.SObjectType, accountItem.Source__c) != null
			) {
				accountItem.RecordTypeId = Utility.getRecordTypeIdByDeveloperName(Account.SObjectType, accountItem.Source__c);
			}
		}
	}
	public static void preventDuplicateRecords(List<Account> accounts) {
		Map<String, Account> emailToAccount = new Map<String, Account>();
		for (Account accountItem :accounts) {
			if (accountItem.Source__c != 'A' && accountItem.PersonEmail != null) {
				if (!emailToAccount.containsKey(accountItem.PersonEmail)) {
					emailToAccount.put(accountItem.PersonEmail, accountItem);
				} else {
					accountItem.PersonEmail.addError(System.Label.Account_already_exists_with_this_email);
				}
			}
		}
		if (!emailToAccount.isEmpty()) {
			List<Account> existAccouts = [SELECT Id, PersonEmail FROM Account WHERE PersonEmail IN :emailToAccount.keySet()];
			Set<String> existEmails = new Set<String>();
			for (Account accountItem :existAccouts) {
				existEmails.add(accountItem.PersonEmail);
			}
			for (Account accountItem :emailToAccount.values()) {
				if (existEmails.contains(accountItem.PersonEmail)) {
					accountItem.PersonEmail.addError(System.Label.Account_already_exists_with_this_email);
				}
			}
		}
	}
}
