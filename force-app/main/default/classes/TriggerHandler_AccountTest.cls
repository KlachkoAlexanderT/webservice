@IsTest
private class TriggerHandler_AccountTest {
	static testmethod void setRecordTypeTest() {
		Account accountA = new Account();
		accountA.Source__c = 'A';
		accountA.FirstName = 'A-Test';
		accountA.LastName = 'A-Test';
		Account accountB = new Account();
		accountB.Source__c = 'B';
		accountB.FirstName = 'B-Test';
		accountB.LastName = 'B-Test';
		Account accountC = new Account();
		accountC.Source__c = 'C';
		accountC.FirstName = 'С-Test';
		accountC.LastName = 'С-Test';
		Account accountE = new Account();
		accountE.FirstName = 'Empty-Test';
		accountE.LastName = 'Empty-Test';
		Account accountP = new Account();
		accountP.LastName = 'Personal';
		accountP.FirstName = 'Empty-Test';
		accountP.RecordTypeId = Utility.getRecordTypeIdByDeveloperName(Account.SObjectType, 'PersonAccount');
		Account accountBs = new Account();
		accountBs.Name = 'Empty-Test';
		accountBs.RecordTypeId = Utility.getRecordTypeIdByDeveloperName(Account.SObjectType, 'Business_Account');
		insert new List<Account>{accountA, accountB, accountC, accountE, accountP, accountBs};
		Map<Id, Account> idToAccount = new Map<Id, Account>([SELECT Id, RecordTypeId FROM Account]);
		System.assertEquals(6, idToAccount.size());
		System.assertEquals(idToAccount.get(accountA.Id).RecordTypeId, Utility.getRecordTypeIdByDeveloperName(Account.SObjectType, 'A'));
		System.assertEquals(idToAccount.get(accountB.Id).RecordTypeId, Utility.getRecordTypeIdByDeveloperName(Account.SObjectType, 'B'));
		System.assertEquals(idToAccount.get(accountC.Id).RecordTypeId, Utility.getRecordTypeIdByDeveloperName(Account.SObjectType, 'C'));
		System.assertEquals(idToAccount.get(accountE.Id).RecordTypeId, Utility.getRecordTypeIdByDeveloperName(Account.SObjectType, 'PersonAccount'));
		System.assertEquals(idToAccount.get(accountP.Id).RecordTypeId, Utility.getRecordTypeIdByDeveloperName(Account.SObjectType, 'PersonAccount'));
		System.assertEquals(idToAccount.get(accountBs.Id).RecordTypeId, Utility.getRecordTypeIdByDeveloperName(Account.SObjectType, 'Business_Account'));
		accountC.Source__c = 'A';
		accountE.Source__c = 'B';
		update new List<Account>{accountC, accountE};
		idToAccount = new Map<Id, Account>([SELECT Id, RecordTypeId FROM Account WHERE Id IN (:accountC.Id, :accountE.Id)]);
		System.assertEquals(idToAccount.get(accountC.Id).RecordTypeId, Utility.getRecordTypeIdByDeveloperName(Account.SObjectType, 'A'));
		System.assertEquals(idToAccount.get(accountE.Id).RecordTypeId, Utility.getRecordTypeIdByDeveloperName(Account.SObjectType, 'B'));
	}

	static testmethod void preventDuplicateRecordsTest() {
		Account accountA1 = new Account();
		accountA1.Source__c = 'A';
		accountA1.FirstName = 'A-Test';
		accountA1.LastName = 'A-Test';
		accountA1.PersonEmail = 'a@gmail.com';
		Account accountA1Copy = new Account();
		accountA1Copy.Source__c = 'A';
		accountA1Copy.FirstName = 'ACopy-Test';
		accountA1Copy.LastName = 'ACopy-Test';
		accountA1Copy.PersonEmail = 'a@gmail.com';
		Account accountA1Copy2 = new Account();
		accountA1Copy2.Source__c = 'A';
		accountA1Copy2.FirstName = 'ACopy-Test';
		accountA1Copy2.LastName = 'ACopy-Test';
		accountA1Copy2.PersonEmail = 'a@gmail.com';
		insert new List<Account>{accountA1, accountA1Copy, accountA1Copy2};
		Boolean flag = true;
		Account accountB1 = new Account();
		accountB1.Source__c = 'B';
		accountB1.FirstName = 'B-Test';
		accountB1.LastName = 'B-Test';
		accountB1.PersonEmail = 'b@gmail.com';
		Account accountB1Copy = new Account();
		accountB1Copy.Source__c = 'B';
		accountB1Copy.FirstName = 'BCopy-Test';
		accountB1Copy.LastName = 'BCopy-Test';
		accountB1Copy.PersonEmail = 'b@gmail.com';
		try {
			insert new List<Account>{accountB1, accountB1Copy};
			flag = false;
		} catch (System.DmlException ex) {
			System.assertEquals(System.Label.Account_already_exists_with_this_email, ex.getDmlMessage(0));
		}
		System.assert(flag);
		insert accountB1;
		System.assertEquals(1, [SELECT Id, RecordTypeId FROM Account WHERE Source__c = 'B'].size());
		try {
			insert accountB1Copy;
			flag = false;
		} catch (System.DmlException ex) {
			System.assertEquals(System.Label.Account_already_exists_with_this_email, ex.getDmlMessage(0));
		}
		System.assert(flag);
		accountB1Copy.Source__c = 'C';
		try {
			insert accountB1Copy;
			flag = false;
		} catch (System.DmlException ex) {
			System.assertEquals(System.Label.Account_already_exists_with_this_email, ex.getDmlMessage(0));
		}
		System.assert(flag);
		accountB1Copy.Source__c = 'A';
		insert accountB1Copy;
		System.assertEquals(2, [SELECT Id, RecordTypeId FROM Account WHERE PersonEmail = 'b@gmail.com'].size());
	}
}
