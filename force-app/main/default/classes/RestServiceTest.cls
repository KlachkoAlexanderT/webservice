@IsTest
private class RestServiceTest {
	static testmethod void setRecordTypeTest() {
		Bonus__c bonusRU = new Bonus__c();
		bonusRU.Ext_Id__c = 'BONUS_RU';
		bonusRU.Region__c = 'RU';
		Bonus__c bonusUA = new Bonus__c();
		bonusUA.Ext_Id__c = 'BONUS_UA';
		bonusUA.Region__c = 'UA';
		Bonus__c bonusKZ = new Bonus__c();
		bonusKZ.Ext_Id__c = 'BONUS_KZ';
		bonusKZ.Region__c = 'KZ';
		Bonus__c bonusBLR = new Bonus__c();
		bonusBLR.Ext_Id__c = 'BONUS_BLR';
		bonusBLR.Region__c = 'BLR';
		Bonus__c bonusBLR_111 = new Bonus__c();
		bonusBLR_111.Ext_Id__c = 'exBLR_111';
		bonusBLR_111.Region__c = 'BLR';
		Bonus__c bonusBLR_222 = new Bonus__c();
		bonusBLR_222.Ext_Id__c = 'exBLR_222';
		bonusBLR_222.Region__c = 'BLR';
		Bonus__c bonusBLR_333 = new Bonus__c();
		bonusBLR_333.Ext_Id__c = 'exBLR_333';
		bonusBLR_333.Region__c = 'BLR';
		Bonus__c bonusRU_111 = new Bonus__c();
		bonusRU_111.Ext_Id__c = 'exRU_111';
		bonusRU_111.Region__c = 'RU';
		Bonus__c bonusRU_222 = new Bonus__c();
		bonusRU_222.Ext_Id__c = 'exRU_222';
		bonusRU_222.Region__c = 'RU';
		Bonus__c bonusRU_333 = new Bonus__c();
		bonusRU_333.Ext_Id__c = 'exRU_333';
		bonusRU_333.Region__c = 'RU';
		Bonus__c bonusUA_111 = new Bonus__c();
		bonusUA_111.Ext_Id__c = 'exUA_111';
		bonusUA_111.Region__c = 'UA';
		Bonus__c bonusUA_222 = new Bonus__c();
		bonusUA_222.Ext_Id__c = 'exUA_222';
		bonusUA_222.Region__c = 'UA';
		Bonus__c bonusUA_333 = new Bonus__c();
		bonusUA_333.Ext_Id__c = 'exUA_333';
		bonusUA_333.Region__c = 'UA';
		Bonus__c bonusKZ_111 = new Bonus__c();
		bonusKZ_111.Ext_Id__c = 'exKZ_111';
		bonusKZ_111.Region__c = 'KZ';
		insert new List<Bonus__c> {
			bonusRU, bonusUA, bonusKZ, bonusBLR, bonusBLR_111, bonusBLR_222, bonusBLR_333,
			bonusUA_111, bonusUA_222, bonusUA_333, bonusRU_111, bonusRU_222, bonusRU_333, bonusKZ_111
		};
		Account accountA = new Account();
		accountA.Source__c = 'C';
		accountA.FirstName = 'A-Test';
		accountA.TransactionID__c = 'A-TransactionID';
		accountA.PersonEmail = 'a@gmail.com';
		accountA.LastName = 'A-Test';
		Account accountB = new Account();
		accountB.Source__c = 'C';
		accountB.FirstName = 'B-Test';
		accountB.TransactionID__c = 'B-TransactionID';
		accountB.PersonEmail = 'b@gmail.com';
		accountB.LastName = 'B-Test';
		insert new List<Account> {
			accountA, accountB
		};
		PersonalAccountToBonus__c personalAccountToBonusA = new PersonalAccountToBonus__c();
		personalAccountToBonusA.Account__c = accountA.Id;
		personalAccountToBonusA.Bonus__c = bonusUA_111.Id;
		insert new List<PersonalAccountToBonus__c> {
			personalAccountToBonusA
		};

		Map<Id, Account> idToAccount = new Map<Id, Account>([SELECT Id, PersonContactId FROM Account]);
		String requestBody = 
		'{' +
			'"source":"C",' +
			'"clients":[' +
				'{' +
					'"clientID":"' + idToAccount.get(accountA.Id).PersonContactId + '",' +
					'"transactionID":"' + accountA.TransactionID__c + '",' +
					'"bonusKey":"' + bonusUA_111.Ext_Id__c + '",' +
					'"lastname": "A-Test1",' + 
					'"firstname": "A-Test2",' + 
					'"email": "a@gmail.com",' + 
					'"mobilePhone": "+2365178951",' + 
					'"birthdate": "2020-10-02",' + 
					'"postalcode": "224000",' + 
					'"country": "Belarus",' + 
					'"city": "Brest",' + 
					'"street": "Lenina"' +
				'},' +
				'{' +
				'"clientID":"' + idToAccount.get(accountB.Id).PersonContactId + '",' +
					'"transactionID":"' + accountB.TransactionID__c + '",' +
					'"bonusKey":"' + bonusBLR_111.Ext_Id__c + '",' +
					'"lastname": "B-Test1",' + 
					'"firstname": "B-Test2",' + 
					'"email": "b@gmail.com"' + 
				'},' +
				'{' +
					'"clientID":"",' +
					'"transactionID":"C-TransactionID",' +
					'"bonusKey":"' + bonusBLR_333.Ext_Id__c + '",' +
					'"lastname": "C-Test1",' + 
					'"firstname": "C-Test2",' + 
					'"email": "a@gmail.com",' + 
					'"mobilePhone": "+2365178951",' + 
					'"birthdate": "2020-10-02",' + 
					'"postalcode": "224000",' + 
					'"country": "Belarus",' + 
					'"city": "Brest",' + 
					'"street": "Lenina"' +
				'},' +
				'{' +
					'"clientID":"",' +
					'"transactionID":"D-TransactionID",' +
					'"bonusKey":"' + bonusKZ_111.Ext_Id__c + '",' +
					'"lastname": "D-Test1",' + 
					'"firstname": "D-Test2",' + 
					'"email": "d@gmail.com"' + 
				'},' +
				'{' +
					'"clientID":"",' +
					'"transactionID":"E-TransactionID",' +
					'"lastname": "E-Test1",' + 
					'"firstname": "E-Test2",' + 
					'"bonusKey":"' + bonusRU_222.Ext_Id__c + '",' +
					'"email": "e@gmail.com"' + 
				'}' +
			']' +
		'}';
		Test.startTest();
		Map<String, Object> result = RestService.upsertAccounts(requestBody);
		Test.stopTest();
		System.assertEquals('5 clients were tried to be inserted/updated. 1 with error', (String)result.get('resultMessage'));
		List<Account> accounts = [
			SELECT Id, PersonContactId, TransactionID__c, LastName, FirstName, PersonMailingCity, PersonEmail, PersonMobilePhone, 
				PersonMailingPostalCode, PersonMailingStreet, (SELECT Id, Bonus__c FROM PersonalAccountsToBonuses__r)
			FROM Account
		];
		Map<String, Account> transactionIDToAccount = new Map<String, Account>();
		for (Account account :accounts) {
			transactionIDToAccount.put(account.TransactionID__c, account);
		}
		List<Object> clients = (List<Object>)result.get('clients');
		Integer countError = 0;
		for (Object client :clients) {
			Map<String, Object> nameToValue = (Map<String, Object>)client;
			if ((Integer) nameToValue.get('resultCode') == 302) {
				System.assert((String.valueOf(nameToValue.get('resultMessage'))).contains(System.Label.Account_already_exists_with_this_email));
				countError++;
			} else {
				String transactionID = (String) nameToValue.get('transactionID');
				switch on transactionID {
					when 'A-TransactionID' {
						System.assertEquals('A-Test1', transactionIDToAccount.get(transactionID).LastName);
						System.assertEquals('A-Test2', transactionIDToAccount.get(transactionID).FirstName);
						System.assertEquals('Brest', transactionIDToAccount.get(transactionID).PersonMailingCity);
						System.assertEquals('a@gmail.com', transactionIDToAccount.get(transactionID).PersonEmail);
						System.assertEquals('+2365178951', transactionIDToAccount.get(transactionID).PersonMobilePhone);
						System.assertEquals('224000', transactionIDToAccount.get(transactionID).PersonMailingPostalCode);
						System.assertEquals('Lenina', transactionIDToAccount.get(transactionID).PersonMailingStreet);
						System.assertEquals(1, transactionIDToAccount.get(transactionID).PersonalAccountsToBonuses__r.size());
						Set<Id> bonuses = new Set<Id> {
							transactionIDToAccount.get(transactionID).PersonalAccountsToBonuses__r[0].Bonus__c
						};
						System.assert(bonuses.contains(bonusUA_111.Id));
					}
					when 'B-TransactionID' {
						System.assertEquals('B-Test1', transactionIDToAccount.get(transactionID).LastName);
						System.assertEquals('B-Test2', transactionIDToAccount.get(transactionID).FirstName);
						System.assertEquals('b@gmail.com', transactionIDToAccount.get(transactionID).PersonEmail);
						System.assertEquals(2, transactionIDToAccount.get(transactionID).PersonalAccountsToBonuses__r.size());
						Set<Id> bonuses = new Set<Id> {
							transactionIDToAccount.get(transactionID).PersonalAccountsToBonuses__r[0].Bonus__c,
							transactionIDToAccount.get(transactionID).PersonalAccountsToBonuses__r[1].Bonus__c
						};
						System.assert(bonuses.contains(bonusBLR_111.Id));
						System.assert(bonuses.contains(bonusBLR.Id));
					}
					when 'D-TransactionID' {
						System.assertEquals('D-Test1', transactionIDToAccount.get(transactionID).LastName);
						System.assertEquals('D-Test2', transactionIDToAccount.get(transactionID).FirstName);
						System.assertEquals('d@gmail.com', transactionIDToAccount.get(transactionID).PersonEmail);
						System.assertEquals(2, transactionIDToAccount.get(transactionID).PersonalAccountsToBonuses__r.size());
						Set<Id> bonuses = new Set<Id> {
							transactionIDToAccount.get(transactionID).PersonalAccountsToBonuses__r[0].Bonus__c,
							transactionIDToAccount.get(transactionID).PersonalAccountsToBonuses__r[1].Bonus__c
						};
						System.assert(bonuses.contains(bonusKZ_111.Id));
						System.assert(bonuses.contains(bonusKZ.Id));
					}
					when 'E-TransactionID' {
						System.assertEquals('E-Test1', transactionIDToAccount.get(transactionID).LastName);
						System.assertEquals('E-Test2', transactionIDToAccount.get(transactionID).FirstName);
						System.assertEquals('e@gmail.com', transactionIDToAccount.get(transactionID).PersonEmail);
						System.assertEquals(2, transactionIDToAccount.get(transactionID).PersonalAccountsToBonuses__r.size());
						Set<Id> bonuses = new Set<Id> {
							transactionIDToAccount.get(transactionID).PersonalAccountsToBonuses__r[0].Bonus__c,
							transactionIDToAccount.get(transactionID).PersonalAccountsToBonuses__r[1].Bonus__c
						};
						System.assert(bonuses.contains(bonusRU_222.Id));
						System.assert(bonuses.contains(bonusRU.Id));
					}
				}
			}
		}
		System.assertEquals(1, countError);
	}
}
