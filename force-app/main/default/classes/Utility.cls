public with sharing class Utility {

	private static Map<Schema.SObjectType,Map<String,Id>> rtypesCache = new Map<Schema.SObjectType, Map<String,Id>>();

	public static Id getRecordTypeIdByDeveloperName(Schema.SObjectType token, String developerName) {
		Map<String, Id> mapRecordTypes = Utility.getRecordTypeMapForObjectGeneric(token);
		if (mapRecordTypes == null) {
			return null;
		} else {
			return mapRecordTypes.get(developerName);
		}
	}

	private static Map<String, Id> getRecordTypeMapForObjectGeneric(Schema.SObjectType token) {
		Map<String, Id> mapRecordTypes = rtypesCache.get(token);
		if (mapRecordTypes != null) {
			return mapRecordTypes;
		} else {
			mapRecordTypes = new Map<String, Id>();
			rtypesCache.put(token, mapRecordTypes);
			Schema.DescribeSObjectResult obj = token.getDescribe();
			Map<String, Schema.RecordTypeInfo> recordTypeInfos = obj.getRecordTypeInfosByDeveloperName();
			for (String rtDevName : recordTypeInfos.keySet()) {
				mapRecordTypes.put(rtDevName, recordTypeInfos.get(rtDevName).recordTypeId);
			}
		}
		return mapRecordTypes;
	}
	public static Set<String> getAPINamesPicklist(Schema.SObjectField picklist) {
		Set<String> apiNamePicklist = new Set<String>();
		if (picklist == null) {
			return apiNamePicklist;
		}
		for (Schema.PicklistEntry picklistValue : picklist.getDescribe().getPicklistValues()) {
			apiNamePicklist.add(picklistValue.getValue());
		}
		return apiNamePicklist;
	}
}
