trigger AccountTrigger on Account (before insert, before update) {
	if (trigger.isBefore && trigger.isInsert) {
		TriggerHandler_Account.setRecordType((List<Account>) Trigger.new, null);
		TriggerHandler_Account.preventDuplicateRecords((List<Account>) Trigger.new);
	}
	if (trigger.isBefore && trigger.isUpdate) {
		TriggerHandler_Account.setRecordType((List<Account>) Trigger.new, (Map<Id, Account>) Trigger.oldMap);
	}
}